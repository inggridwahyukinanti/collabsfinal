<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User','user_id');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag','post_tag','post_id','tag_id');
    }

    public function comments(){
        return $this->belongsToMany('App\Comment','post_comment','post_id','comment_id');
    }
}
