<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CommentsController extends Controller
{
        public function create()
    {
        return view('comments.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'body' => 'required|unique:create',
        ]);

        $query = DB::table('comments')->insert([
            "body" => $request["body"],
        ]);


        return redirect('/comments');
    }

    public function index()
    {
        $comments = DB::table('comments')->get();
        return view('comments.index', compact('comments'));
    }
    public function show($id)
    {
        $comments = DB::table('comments')->where('id', $id)->first();
        return view('comments.show', compact('comments'));
    }

    public function edit($id)
    {
        $create = DB::table('comments')->where('id', $id)->first();
        return view('comments.edit', compact('comments'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'body' => 'required|unique:create',
        ]);

        $query = DB::table('comments')
            ->where('id', $id)
            ->update([
            "body" => $request["body"],
            ]);
        return redirect('/comments');
    }

    public function destroy($id)
    {
        $query = DB::table('comments')->where('id', $id)->delete();
        return redirect('/comments');
    }
}
