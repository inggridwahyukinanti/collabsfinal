<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TagsController extends Controller
{
    public function create()
    {
        return view('tags.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'tag_name' => 'required|unique:create'
        ]);

        $query = DB::table('tags')->insert([
            "tag_name" => $request["tag_name"]
        ]);


        return redirect('/tags');
    }

    public function index()
    {
        $tags = DB::table('tags')->get();
        return view('tags.index', compact('tags'));
    }
    public function show($id)
    {
        $tags = DB::table('tags')->where('id', $id)->first();
        return view('tags.show', compact('tags'));
    }

    public function edit($id)
    {
        $create = DB::table('tags')->where('id', $id)->first();
        return view('tags.edit', compact('tags'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'tag_name' => 'required|unique:create'
        ]);

        $query = DB::table('tags')
            ->where('id', $id)
            ->update([
            "tag_name" => $request["tag_name"]
            ]);
        return redirect('/tags');
    }

    public function destroy($id)
    {
        $query = DB::table('tags')->where('id', $id)->delete();
        return redirect('/tags');
    }
}
