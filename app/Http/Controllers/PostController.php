<?php

namespace App\Http\Controllers;
use DB;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use App\Tag;
use App\Exports\PostsExport;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $posts = $user->posts;
        //dd($posts);
        //$posts = Post::all();
         return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'caption' => 'required',
            'quote' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);

        $imgName = $request->image->getClientOriginalName().'-'.time().'.'.$request->image->extension();
        $request->image->move(public_path('image'), $imgName);

        $tags_arr = explode(',',$request["tags"]);
        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::where("tag_name",$tag_name)->first();
            if($tag){
                $tag_ids[] = $tag->id;
            } else {
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }
        $post = Post::create([
            "title" => $request["title"],
            "caption" => $request["caption"],
            "quote" => $request["quote"],
            'image' => $imgName,
            "user_id" => Auth::id()
        ]);

        $post->tags()->sync($tag_ids);
        //$user = Auth::user();
        //$user->posts()->save($post);

        Alert::success('Berhasil', 'Berhasil menambah Postingan');

        return redirect('/posts')->with('success', 'Post berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Post::find($id);

        return view('posts.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Post::find($id);
        return view('posts.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'caption' => 'required',
            'quote' => 'required'
            //'image' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);

        //$imgName = $request->image->getClientOriginalName().'-'.time().'.'.$request->image->extension();
       // $request->image->move(public_path('image'), $imgName);

       // $tags_arr = explode(',',$request["tags"]);
       // $tag_ids = [];
       // foreach($tags_arr as $tag_name){
       //     $tag = Tag::where("tag_name",$tag_name)->first();
       //     if($tag){
       //         $tag_ids[] = $tag->id;
       //     } else {
       //         $new_tag = Tag::create(["tag_name" => $tag_name]);
       //         $tag_ids[] = $new_tag->id;
       //     }
       // }

        $update = Post::where('id', $id)->update([
            "title" => $request["title"],
            "caption" => $request["caption"],
            "quote" => $request["quote"]
            //"image" => $imgName
            ]);
       //  $update->tags()->sync($tag_ids);
        return redirect('/posts')->with('success', 'Berhasil update Post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/posts')->with('success', 'post berhasil di hapus!');
    }

    public function export()
    {
        return Excel::download(new PostsExport, 'posts.xlsx');
    }
}
