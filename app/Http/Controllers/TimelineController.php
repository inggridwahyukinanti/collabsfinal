<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class TimelineController extends Controller
{
    public function index(){
        $posts = Post::all();
        //dd($posts);
         return view('timelines.index', compact('posts'));
    }
    public function show($id)
    {
        $id = Post::find($id);

        return view('posts.show', compact('id'));
    }
}
