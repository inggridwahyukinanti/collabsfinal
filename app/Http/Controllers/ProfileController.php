<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $profiles = $user->profiles;
        //$profiles = profile::all();
         return view('profiles.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        $profile = profile::create([
            "fullname" => $request["fullname"],
            "address" => $request["address"],
            "phone" => $request["phone"],
           "user_id" => Auth::id()
        ]);

        //$user = Auth::user();
        //$user->profiles()->save($profile);

        return redirect('/profiles/{$profile->id}')->with('success', 'profile berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = profile::find($id);
        return view('profiles.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = profile::find($id);
        return view('profiles.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fullname' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        $update = profile::where('id', $id)->update([
            "fullname" => $request["fullname"],
            "address" => $request["address"],
            "phone" => $request["phone"]
            ]);
        return redirect('/profiles')->with('success', 'Berhasil update profile!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        profile::destroy($id);
        return redirect('/profiles')->with('success', 'profile berhasil di hapus!');
    }
}
