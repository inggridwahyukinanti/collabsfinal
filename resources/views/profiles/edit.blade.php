@include('template.navbar')

<div class="card card-primary ml-3 mt-3">
    <div class="card-header">
      <h3 class="card-title">Edit My Profile {{$id->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/profiles/{{$id->id}}" method="POST">
    @csrf
    @method('PUT')
      <div class="card-body">
      <div>
        <div class="form-group">
          <label for="exampleInputfull_name1">full_name</label>
          <input type="text" class="form-control" id="exampleInputfull_name1" name="full_name" value=" {{ old('full_name', $id->full_name)}}" placeholder="Enter full_name">
              @error('full_name')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputaddress1">address</label>
          <input type="text" class="form-control" id="exampleInputaddress1" name="address" value=" {{ old('address', $id->address)}}" placeholder="address">
              @error('address')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputphone1">phone</label>
          <input type="text" class="form-control" id="exampleInputphone1" name="phone" value=" {{ old('phone', $id->phone)}}" placeholder="phone">
              @error('phone')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
</div>
@include('template.footer')
