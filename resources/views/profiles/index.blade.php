@include('template.navbar')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">profiles Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
              @endif
              <a class="btn btn-primary" href=" {{ route('profiles.create') }} ">Create a New profile</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">User ID</th>
                      <th>Title</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @forelse($profiles as $key => $profile)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $profile->fullname }}</td>
                    <td style="display: flex;">
                    <a href=" {{ route('profiles.show', ['profile' => $profile->id])}} " class="btn btn-info btn-sm">show</a>
                    <a href="/profiles/{{$profile->id}}/edit" class="btn btn-default btn-sm">edit</a>
                    <form action="/profiles/{{$profile->id}}" method="profile">
                    @csrf
                    @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@include('template.footer')
