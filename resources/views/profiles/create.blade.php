@include('template.navbar')

<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Buat sebuah profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/profiles" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputfullname1">fullname</label>
                    <input type="fullname" class="form-control" id="exampleInputfullname1" name="fullname" value=" {{ old('fullname', '')}}" placeholder="Enter fullname">
                        @error('fullname')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputaddress1">address</label>
                    <input type="address" class="form-control" id="exampleInputaddress1" name="address" value=" {{ old('address', '')}}" placeholder="Enter address">
                        @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputaddress1">phone</label>
                    <input type="text" class="form-control" id="exampleInputaddress1" name="phone" value=" {{ old('phone', '')}}" placeholder="Enter phone">
                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
</div>
@include('template.footer')
