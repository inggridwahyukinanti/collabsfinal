@include('template.navbar')


<div class="card card-primary mt-3 ml-3">

    <a class="btn btn-primary" href="profiles/{$profile->id}/edit">Edit My Profile</a>
    <p>Account : {{ Auth::user()->username }}</p>
</div>

@include('template.footer')
