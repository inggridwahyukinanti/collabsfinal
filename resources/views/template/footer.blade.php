<!--<div class="update_section">
    <div class="container">
       <h1 class="update_taital">Get Every Update.... </h1>
       <form action="/action_page.php">
          <div class="form-group">
              <textarea class="update_mail" placeholder="Massage" rows="5" id="comment" name="Massage"></textarea>
          </div>
          <div class="subscribe_bt"><a href="#">Subscribe Now</a></div>
       </form>
    </div>
 </div>-->
 <!-- update section end -->
 <!-- footer section start -->
 <div class="footer_section layout_padding">
    <div class="container">
       <div class="footer_section_2">
          <div class="row">
             <div class="col-lg-3 col-sm-6">
                <h2 class="useful_text">Anggota</h2>
                <div class="footer_menu">
                   <ul>
                      <li><a href="#">Dikdik</a></li>
                      <li><a href="#">Inggrid</a></li>
                      <li><a href="#">Yockie</a></li>
                   </ul>
                </div>
             </div>
             <div class="col-lg-3 col-sm-6">
                <h2 class="useful_text">About</h2>
                <p class="footer_text">Project ini diperuntukan sebagai tugas akhir dari yang diberikan oleh Sanbercode. Walapun masih belum sempurna, tetapi kami telah berjuang semampu kami. Terimakasih Team Sanbercode</p>
             </div>
             <div class="col-lg-3 col-sm-6">
                <h2 class="useful_text">Sanbercode Info</h2>
                <div class="location_text">
                   <ul>
                      <li>
                         <a href=https://sanbercode.com><i class="fa fa-map-marker" aria-hidden="true"></i>
                         <span class="padding_15">Sanbercode</span></a>
                      </li>
                      <li>
                         <a href=https://www.instagram.com/sanbercode/><i class="fa fa-phone" aria-hidden="true"></i>
                         <span class="padding_15">Instagram</span></a>
                      </li>
                      <li>
                         <i class="fa fa-envelope" aria-hidden="true"></i>
                         <span class="padding_15">dikdikmusfarr@gmail.com</span></a>
                      </li>
                   </ul>
                </div>
             </div>
             <div class="col-lg-3 col-sm-6">
                <h2 class="useful_text">countrys</h2>
                <div class="map_image"><img src="images/map-bg.png"></div>
             </div>
          </div>
       </div>
    </div>
 </div>
 <!-- footer section end -->
 <!-- copyright section start -->
 <div class="copyright_section">
    <div class="container">
       <div class="row">
          <div class="col-sm-12">
             <p class="copyright_text">© 2020 All Rights Reserved.<a href="https://html.design"> Thanks to this Template</a></p>
          </div>
       </div>
    </div>
 </div>
 <!-- copyright section end -->
 <!-- Javascript files-->
 <script src="js/jquery.min.js"></script>
 <script src="js/popper.min.js"></script>
 <script src="js/bootstrap.bundle.min.js"></script>
 <script src="js/jquery-3.0.0.min.js"></script>
 <script src="js/plugin.js"></script>
 <!-- sidebar -->
 <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
 <script src="js/custom.js"></script>
 <!-- javascript -->
 <script src="js/owl.carousel.js"></script>
 <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
 <script>
    $(document).ready(function(){
    $(".fancybox").fancybox({
    openEffect: "none",
    closeEffect: "none"
    });

    $(".zoom").hover(function(){

    $(this).addClass('transition');
    }, function(){

    $(this).removeClass('transition');
    });
    });
 </script>
 <script>
    function openNav() {
    document.getElementById("myNav").style.width = "100%";
    }
    function closeNav() {
    document.getElementById("myNav").style.width = "0%";
    }
 </script>
</body>
</html>
