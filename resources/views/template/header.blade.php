<div class="container">
    <div id="my_slider" class="carousel slide" data-ride="carousel">
       <div class="carousel-inner">
          <div class="carousel-item active">
             <div class="row">
                <div class="col-md-6">
                   <div class="container">
                      <h1 class="banner_taital">Dikdik Musfar</h1>
                      <p class="banner_text">Wah gila sih Sanbercode seru banget. Bener-bener nge push kita buat nugas tiap hari dan sangat worth. Terimakasih telah membuat aku keluar dari zona nyaman dan juga membuat waktu kita lebih produktif.</p>
                      <div class="more_bt"><a href=https://instagram.com/primusx>Read More</a></div>
                   </div>
                </div>
                <div class="col-md-6">
                   <div class="banner_img"><img src="images/dikdik.png"></div>
                </div>
             </div>
          </div>
          <div class="carousel-item">
             <div class="row">
                <div class="col-md-6">
                   <div class="container">
                      <h1 class="banner_taital">Inggrid Wahyu Kinanti</h1>
                      <p class="banner_text">Mantul pisan si SnberCode euy...seru abis pokoknya. Cocok buat tipe orang yang belajar gak materi aja tapi langsung praktek + yang suka tantangan, jadi double kill ilmunya. Terimakasih SanberCode udah membantu saya memahami cara membuat web yang benar ehehehe sukses selalu...</p>
                      <div class="more_bt"><a href="#">Read More</a></div>
                   </div>
                </div>
                <div class="col-md-6">
                   <div class="banner_img"><img src="images/banner-img.png"></div>
                </div>
             </div>
          </div>
          <div class="carousel-item">
             <div class="row">
                <div class="col-md-6">
                   <div class="container">
                      <h1 class="banner_taital">Yockie Taupan Putra</h1>
                      <p class="banner_text">Asik ni ikutan bootcamp disanbercode, dimana pelajaran yg diberikan mulai dari yang paling dasar, saya yg belum mengenal web programming bisa langsung memahaminya dengan mudah. semangat ini langkah awal untuk mulai mendalami dan menyelami keilmuan web programming. sukses buat sanbrecode. dan terima kasih kepada teman teman seperjuangan terutama teman kelompok tugas akhir..</p>
                      <div class="more_bt"><a href="#">Read More</a></div>
                   </div>
                </div>
                <div class="col-md-6">
                   <div class="banner_img"><img src="images/banner-img.png"></div>
                </div>
             </div>
          </div>
       </div>
       <a class="carousel-control-prev" href="#my_slider" role="button" data-slide="prev">
       <i class="fa fa-angle-left"></i>
       </a>
       <a class="carousel-control-next" href="#my_slider" role="button" data-slide="next">
       <i class="fa fa-angle-right"></i>
       </a>
    </div>
 </div>
