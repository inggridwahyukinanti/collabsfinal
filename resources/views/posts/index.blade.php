@include('template.navbar')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">posts Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
              @endif
              <a class="btn btn-primary" href=" {{ route('posts.create') }} ">Create a New post</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">Number</th>
                      <th>Title</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @forelse($posts as $key => $post)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $post->title }}</td>
                    <td style="display: flex;">
                    <a href=" {{ route('posts.show', ['post' => $post->id])}} " class="btn btn-info btn-sm">show</a>
                    <a href="/posts/{{$post->id}}/edit" class="btn btn-default btn-sm">edit</a>
                    <form action="/posts/{{$post->id}}" method="post">
                    @csrf
                    @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@include('template.footer')
