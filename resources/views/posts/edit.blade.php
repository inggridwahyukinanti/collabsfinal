@include('template.navbar')

<div class="card card-primary ml-3 mt-3">
    <div class="card-header">
      <h3 class="card-title">Edit a post {{$id->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/posts/{{$id->id}}" method="POST">
    @csrf
    @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputtitle1">Title</label>
          <input type="title" class="form-control" id="exampleInputtitle1" name="title" value=" {{ old('title', $id->title)}}" placeholder="Enter title">
              @error('title')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>
      <div>
        <div class="form-group">
          <label for="exampleInputcaption1">Caption</label>
          <input type="caption" class="form-control" id="exampleInputcaption1" name="caption" value=" {{ old('caption', $id->caption)}}" placeholder="Enter caption">
              @error('caption')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputquote1">Quote</label>
          <input type="quote" class="form-control" id="exampleInputquote1" name="quote" value=" {{ old('quote', $id->quote)}}" placeholder="quote">
              @error('quote')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>
        <!-- Edit Image -->
        <!--<div class="form-group">
          <label for="image">image</label>
          <input type="file" class="form-control" id="image" name="image" value=" {{ old('image', $id->image)}}" placeholder="image">
              @error('image')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
        </div>-->
        <!-- Edit Tags -->
        <!--<div class="form-group">
            <label for="exampleInputtag1">Tags</label>
            <input type="text" class="form-control" id="tags" name="tags" value="{{old('tags',$id->tags)}}" placeholder="Pisahkan dengan koma">
            </div>
        </div>-->
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
</div>
@include('template.footer')
