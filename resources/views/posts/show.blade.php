@include('template.navbar')


<div class="card card-primary mt-3 ml-3 mr-3 ">
    <img class="card-img-top" src="/image/{{$id->image}}" alt="">
    <div class="d-flex justify-content-center">
        <h4>The Post is {{ $id->title }} </h4>
    </div>
    <div class="d-flex justify-content-center">
        <p>{{$id->caption}}</p>
    </div>
    <div class="d-flex justify-content-center">
        <q>{{$id->quote}}</q>
    </div>
    <div class="d-flex justify-content-center">
        <p>Author : {{ $id->author->name }}</p>
    </div>
    <div class="d-flex justify-content-center">
            Tags :
            @forelse ( $id->tags as $tag )
                <button class="btn btn-primary btn-sm mr-2 ml-2"> {{$tag->tag_name}} </button>

                @empty
                No Tags
            @endforelse
    </div>
    <div class="card mt-3">
        <div class="card-body">
            <h5 class="card-title">
                Nama User
            </h5>
            <p class="card-text">
                Komentar User
            </p>
            <a href="" class="btn btn-primary">Kirim</a>
        </div>
    </div>
</div>

@include('template.footer')
