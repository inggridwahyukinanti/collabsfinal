@include('template.navbar')

<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Buat sebuah Postingan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts" method="POST" enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputTitle1">Judul Postingan</label>
                    <input type="name" class="form-control" id="exampleInputTitle1" name="title" value=" {{ old('title', '')}}" placeholder="Enter Title">
                        @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputcaption1">caption</label>
                    <input type="caption" class="form-control" id="exampleInputcaption1" name="caption" value=" {{ old('caption', '')}}" placeholder="Enter caption">
                        @error('caption')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputquote1">quote</label>
                    <input type="quote" class="form-control" id="exampleInputquote1" name="quote" value=" {{ old('quote', '')}}" placeholder="Enter quote">
                        @error('quote')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="image">upload image</label>
                    <input type="file" class="form-control-file" id="image" name="image" value=" {{ old('image', '')}}" placeholder="Enter image">
                        @error('image')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                <div class="form-group">
                    <label for="exampleInputtag1">Tags</label>
                    <input type="text" class="form-control" id="tags" name="tags" value="{{old('tags','')}}" placeholder="Pisahkan dengan koma">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
</div>
@include('template.footer')
