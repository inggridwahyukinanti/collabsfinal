@include('template.navbar')
      <!-- banner section start / Gambar Slider Berjalan -->
      <div class="banner_section layout_padding">
         <div class="container">
            <div id="my_slider" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="container">
                              <h1 class="banner_taital">Cara Menggunakan WEB</h1>
                              <p class="banner_text">buka navbar lalu pilih menu yang diinginkan</p>
                              <div class="more_bt"><a href="#">Read More</a></div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="banner_img"><img src="{{ asset('/template/images/banner-img.png')}}"></div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="container">
                              <h1 class="banner_taital">Get Medical Care early</h1>
                              <p class="banner_text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                              <div class="more_bt"><a href="#">Read More</a></div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="banner_img"><img src="{{ asset('/template/images/banner-img.png')}}"></div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="container">
                              <h1 class="banner_taital">Get Medical Care early</h1>
                              <p class="banner_text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                              <div class="more_bt"><a href="#">Read More</a></div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="banner_img"><img src="{{ asset('/template/images/banner-img.png')}}"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <a class="carousel-control-prev" href="#my_slider" role="button" data-slide="prev">
               <i class="fa fa-angle-left"></i>
               </a>
               <a class="carousel-control-next" href="#my_slider" role="button" data-slide="next">
               <i class="fa fa-angle-right"></i>
               </a>
            </div>
         </div>
      </div>
      <!-- banner section end -->
      </div>
      <!-- header section end -->
      <!-- protect section start -->
      <div class="protect_section layout_padding">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <h1 class="protect_taital">SIAPA SIH COVID-19</h1>
                  <p class="protect_text">Pada kesempatan ini juga kami ingin berbagi pengetahuan tentang Covid-19 dan Pencegahannya. Semoga dapat membantu</p>
               </div>
            </div>
            <div class="protect_section_2 layout_padding">
               <div class="row">
                  <div class="col-md-6">
                     <h1 class="hands_text"><a href="#">infectious disease caused by a newly discovered coronavirus</a></h1>
                     <h1 class="hands_text_2"><a href="#">the disease it causes  and how it spreads</a></h1>
                     <h1 class="hands_text"><a href="#">it’s important that you also nose and practice respiratory etiquette</a></h1>
                  </div>
                  <div class="col-md-6">
                     <div class="image_2"><img src="{{ asset('/template/images/img-2.png')}}"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- protect section end -->
      <!-- about section start -->
      <div class="about_section layout_padding">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <div class="about_img"><img src="{{ asset('/template/images/img-1.png')}}"></div>
               </div>
               <div class="col-md-6">
                  <h1 class="about_taital">Coronavirus what it is?</span></h1>
                  <p class="about_text">You wanna know more about Coronavirus? check this out</p>
                  <div class="read_bt"><a href=https://www.who.int/health-topics/coronavirus#tab=tab_1>Read More</a></div>
               </div>
            </div>
         </div>
      </div>
      <!-- about section end -->
      <!-- doctor section start -->
      <div class="doctors_section layout_padding">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="taital_main">
                     <div class="taital_left">
                        <div class="play_icon"><img src="{{ asset('/template/images/play-icon.png')}}"></div>
                     </div>
                     <div class="taital_right">
                        <h1 class="doctor_taital">What doctors say..</h1>
                        <p class="doctor_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                        <div class="readmore_bt"><a href=https://www.youtube.com/watch?v=3Uugj-zxA5E>Watch</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- doctor section end -->
      <!-- news section start -->
      <div class="news_section layout_padding">
         <div class="container">
            <div id="main_slider" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <h1 class="news_taital">Latest News</h1>
                     <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                     <div class="news_section_2 layout_padding">
                        <div class="box_main">
                           <div class="image_1"><img src="{{ asset('/template/images/news-img.png')}}"></div>
                           <h2 class="design_text">Coronavirus is Very dangerous</h2>
                           <p class="lorem_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                           <div class="read_btn"><a href=https://www.researchgate.net/post/Is-Coronavirus-really-dangerous>Read More</a></div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                    <h1 class="news_taital">Latest News</h1>
                     <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                     <div class="news_section_2 layout_padding">
                        <div class="box_main">
                           <div class="image_1"><img src="{{ asset('/template/images/news-img.png')}}"></div>
                           <h2 class="design_text">Coronavirus is Very dangerous</h2>
                           <p class="lorem_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                           <div class="read_btn"><a href="#">Read More</a></div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                    <h1 class="news_taital">Latest News</h1>
                     <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                     <div class="news_section_2 layout_padding">
                        <div class="box_main">
                           <div class="image_1"><img src="{{ asset('/template/images/news-img.png')}}"></div>
                           <h2 class="design_text">Coronavirus is Very dangerous</h2>
                           <p class="lorem_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                           <div class="read_btn"><a href="#">Read More</a></div>
                        </div>
                     </div>
                  </div>
               </div>
               <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
               <i class="fa fa-angle-left"></i>
               </a>
               <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
               <i class="fa fa-angle-right"></i>
               </a>
            </div>
            </div>
         </div>
      </div>
      <!-- news section end -->
      <!-- update section start -->
      <div class="update_section">
         <div class="container">
            <h1 class="update_taital">Get Every Update.... </h1>
            <form action="/action_page.php">
               <div class="form-group">
                   <textarea class="update_mail" placeholder="Massage" rows="5" id="comment" name="Massage"></textarea>
               </div>
               <div class="subscribe_bt"><a href="#">Subscribe Now</a></div>
            </form>
         </div>
      </div>
      <!-- update section end -->
      <!-- footer section start -->
      <div class="footer_section layout_padding">
         <div class="container">
            <div class="footer_section_2">
               <div class="row">
                  <div class="col-lg-3 col-sm-6">
                     <h2 class="useful_text">Resources</h2>
                     <div class="footer_menu">
                        <ul>
                           <li><a href="#">What we do</a></li>
                           <li><a href="#">Media</a></li>
                           <li><a href="#">Travel Advice</a></li>
                           <li><a href="#">Protection</a></li>
                           <li><a href="#">Care</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                     <h2 class="useful_text">About</h2>
                     <p class="footer_text">CONVID19 is a part of the Indonesian nation's concern in providing health information services about COVID19.</p>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                     <h2 class="useful_text">Contact Us</h2>
                     <div class="location_text">
                        <ul>
                           <li>
                              <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>
                              <span class="padding_15">Location</span></a>
                           </li>
                           <li>
                              <a href="#"><i class="fa fa-phone" aria-hidden="true"></i>
                              <span class="padding_15">Call +62 2234567890</span></a>
                           </li>
                           <li>
                              <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>
                              <span class="padding_15">convidcare@gmail.co.id</span></a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                     <h2 class="useful_text">countrys</h2>
                     <div class="map_image"><img src="{{ asset('/template/images/map-bg.png')}}"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- footer section end -->
      <!-- copyright section start -->
      <div class="copyright_section">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <p class="copyright_text">© 2021 CONVID19<a href="https://html.design"> CARE WITH LOVE </a></p>
               </div>
            </div>
         </div>
      </div>
      <!-- copyright section end -->
      <!-- Javascript files-->
      <script src="{{ asset('/templatejs/jquery.min.js')}}"></script>
      <script src="{{ asset('/template/js/popper.min.js')}}"></script>
      <script src="{{ asset('/template/js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{ asset('/template/js/jquery-3.0.0.min.js')}}"></script>
      <script src="{{ asset('/template/js/plugin.js')}}"></script>
      <!-- sidebar -->
      <script src="{{ asset('/template/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
      <script src="{{ asset('/template/js/custom.js')}}"></script>
      <!-- javascript -->
      <script src="{{ asset('/template/js/owl.carousel.js')}}"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
         openEffect: "none",
         closeEffect: "none"
         });

         $(".zoom").hover(function(){

         $(this).addClass('transition');
         }, function(){

         $(this).removeClass('transition');
         });
         });
      </script>
      <script>
         function openNav() {
         document.getElementById("myNav").style.width = "100%";
         }
         function closeNav() {
         document.getElementById("myNav").style.width = "0%";
         }
      </script>
   </body>
</html>
