@include('template.navbar')
<body>


<div class="banner_section layout_padding">
    @include('template.header')
 </div>
 <!-- banner section end -->
 </div>
 <!-- header section end -->
 <!-- protect section start -->

 <div class="protect_section layout_padding">
     @yield('content')
 </div>
 <!-- protect section end -->

 <!-- doctor section start -->
 <!--<div class="doctors_section layout_padding">
    <div class="container-fluid">
       <div class="row">
          <div class="col-sm-12">
             <div class="taital_main">
                <div class="taital_left">
                   <div class="play_icon"><img src="images/play-icon.png"></div>
                </div>
                <div class="taital_right">
                   <h1 class="doctor_taital">What doctors say..</h1>
                   <p class="doctor_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                   <div class="readmore_bt"><a href="#">Read More</a></div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>-->
 <!-- doctor section end -->
 <!-- news section start -->
 <!--<div class="news_section layout_padding">
    <div class="container">
       <div id="main_slider" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
             <div class="carousel-item active">
                <h1 class="news_taital">Latest News</h1>
                <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                <div class="news_section_2 layout_padding">
                   <div class="box_main">
                      <div class="image_1"><img src="images/news-img.png"></div>
                      <h2 class="design_text">Coronavirus is Very dangerous</h2>
                      <p class="lorem_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                      <div class="read_btn"><a href="#">Read More</a></div>
                   </div>
                </div>
             </div>
             <div class="carousel-item">
               <h1 class="news_taital">Latest News</h1>
                <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                <div class="news_section_2 layout_padding">
                   <div class="box_main">
                      <div class="image_1"><img src="images/news-img.png"></div>
                      <h2 class="design_text">Coronavirus is Very dangerous</h2>
                      <p class="lorem_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                      <div class="read_btn"><a href="#">Read More</a></div>
                   </div>
                </div>
             </div>
             <div class="carousel-item">
               <h1 class="news_taital">Latest News</h1>
                <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                <div class="news_section_2 layout_padding">
                   <div class="box_main">
                      <div class="image_1"><img src="images/news-img.png"></div>
                      <h2 class="design_text">Coronavirus is Very dangerous</h2>
                      <p class="lorem_text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look</p>
                      <div class="read_btn"><a href="#">Read More</a></div>
                   </div>
                </div>
             </div>
          </div>
          <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
          <i class="fa fa-angle-left"></i>
          </a>
          <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
          <i class="fa fa-angle-right"></i>
          </a>
       </div>
       </div>
    </div>
 </div>-->
 
</body>
 @include('template.footer')
