@extends('welcome')

@section('content')
<a class="btn btn-dark ml-3" href="/test-excel">Export All Posts</a>
<a class="btn btn-dark ml-3" href="/test-dompdf-2">Apologize</a>
<a class="btn btn-dark ml-3" href="/test-dompdf">Deficiency</a>
    <!-- Post section start -->
    <div class="card-body">
        @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
        @endif

 <div class="about_section layout_padding">
    <div class="container">
    @forelse($posts as $key => $post)
       <div class="row justify-content-center">
          <div class="col-xl-5 my-3">
             <div class="about_img"><img src="/image/{{$post->image}}"></div>
          </div>
          <div class="col-xl-5 my-3">
             <h3 class="mb-0">{{$post->caption}}</h3>
             <q class="about_quote">{{$post->quote}}</q>
             <p class="about_text">{{ $post->author->name }}</p>
             <div class="read_bt"><a href="/posts/{id}">Say Something</a></div>
          </div>
       </div>
       @empty
        <tr>
            <td colspan="6" align="center"> No Post Yet </td>
        </tr>
    @endforelse
    </div>
 </div>
</div>
 <!-- post section end -->
@endsection
