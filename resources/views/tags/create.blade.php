@extends('master')

@section('content')
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Tag</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/tags" method="POST">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter a Title">
                    @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  </div>
                  </div>
                  <div class="card-body">
                  <div class="form-group">
                    <label for="content">Content</label>
                    <input type="text" class="form-control" id="content" name="content" placeholder="Input Content">
                    @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
      </div>
@endsection