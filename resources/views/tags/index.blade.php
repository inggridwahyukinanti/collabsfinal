@extends('master')

@section('content')
Tags
    <div class="mt-3 ml-3">

<a href="/cast/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">content</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($tags as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->title}}</td>
                        <td>{{$value->content}}</td>
                        <td>
                            <a href="/tags/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/tags/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/tags/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
@endsection