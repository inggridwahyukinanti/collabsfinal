<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BrandaController@index');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', function () {
    return view('home');
});

Route::get('/master', function () {
    return view('master');
});




/* Routes Tags*/
Route::get('/tags', 'TagsController@index');
Route::get('/tags/create', 'TagsController@create');
Route::post('/tags', 'TagsController@store');
Route::get('/tags/{tags_id}', 'TagsController@show');
Route::get('/tags/{tags_id}/edit', 'TagsController@edit');
Route::put('/tags/{tags_id}', 'TagsController@update');
Route::delete('/tags/{tags_id}', 'TagsController@destroy');

/* Routes Comments*/
Route::get('/comments', 'CommentsController@index');
Route::get('/comments/create', 'CommentsController@create');
Route::post('/comments', 'CommentsController@store');
Route::get('/comments/{comments_id}', 'CommentsController@show');
Route::get('/comments/{comments_id}/edit', 'CommentsController@edit');
Route::put('/comments/{comments_id}', 'CommentsController@update');
Route::delete('/comments/{comments_id}', 'CommentsController@destroy');

/* Routes Posts */
Route::resource('posts','PostController')->middleware('auth');

/* Routes Profiles */
//Route::get('/profiles/{profiles_id}', 'ProfileController@show')->middleware('auth');
//Route::get('/profiles/{profiles_id}/edit', 'ProfileController@edit')->middleware('auth');
//Route::put('/profiles/{profiles_id}', 'ProfileController@update')->middleware('auth');
Route::resource('profiles','ProfileController')->middleware('auth');

/* Routes Timelines */
Route::get('/timelines', 'TimelineController@index');
Route::get('/posts/{id}', 'TimelineController@index');

Route::get('/home', function () {
    return view('home');
});

Route::get('/test-dompdf',function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('
    <h1>Selamat Pagi Ka Rezky</h1>
    <p> selesai meriksa tugas ini, tolong bahas atau share source code yang social media ya :( aku bener bener ingin menganalisan website yang berjalan lancar di setiap fungsinya. Aku sangat memohon ka please share ya :((  </p>
    <h3> Web ini belum bisa buka profile.index, tapi kalau create profile bisa :(( </h3>
    <h3> Web ini belum bisa edit tag, tapi kalau create tag bisa :(( </h3>
    <h3> Udah bisa buka timeline ko xixixixi </h3>
    <h3> My Post pun juga bisa </h3>
    <h3> Tapi belum bisa bikin komen :(((((((((( tolong ya ka bagi source code yang sosial media berjalan lancar xixixii </h3>
    <h3> oh iya pengen tau juga cara ngambil data biar bisa dimasukin ke dom pdf </h3>
    ');
    return $pdf->stream();
});

Route::get('/test-dompdf-2','PdfController@print');

Route::get('/test-excel','PostController@export');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
